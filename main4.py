import concurrent.futures

import requests

indexer = "https://indexer.mainnet.aptoslabs.com/v1/graphql"

def get_voting_power(ve_token):
    try:
        url = "http://35.196.206.219:8080/v1/view"
        payload = {
            "type": "entry_function_payload",
            "function": f"0x4bf51972879e3b95c4781a5cdcb9e1ee24ef483e7d22f2d903626f126df62bd1::voting_escrow::get_voting_power",
            "type_arguments": [],
            "arguments": [
                f"{ve_token}",
            ],
        }
        return requests.post(url, json=payload).json()[0]
    except Exception as e:
        print(f"ve_token: {ve_token} - error: {e}")
        return 0


def get_owner(ve_token):
    if len(ve_token) == 65:
        ve_token = '0x0' + ve_token[2:]
    try:
        query = '''query TokenOwnedFromCollectionAddress($storage_id: String!) {
          current_token_ownerships_v2(
            where: {storage_id: {_eq: $storage_id}, amount: {_gt: "0"}}
            limit: 100
            offset: 0
            order_by: {storage_id: asc}
          ) {
            storage_id
            owner_address
          }
        }
        '''

        variables = {
            "storage_id": ve_token,
        }

        response = requests.post(indexer, json={"query": query, "variables": variables})
        return response.json()["data"]["current_token_ownerships_v2"][0]['owner_address']
    except Exception as e:
        print(f"ve_token: {ve_token} - error: {e}")
        return None

def job(ve_token, res):
    print(f"ve_token: {ve_token}")
    voting_power = get_voting_power(ve_token)
    owner = get_owner(ve_token)
    res[ve_token] = {
        "voting_power": voting_power,
        "owner": owner
    }


res = {}

f = open("result.csv", "r")
lines = f.readlines()

with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    _ = [executor.submit(job, line.strip(), res) for line in lines[90000:]]

f = open("result_4.csv", "w")
for key in res:
    f.write(f"{key},{res[key]['voting_power']},{res[key]['owner']}\n")