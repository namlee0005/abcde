import json
import concurrent.futures

import requests

def get_total_voted(ve_token):
    try:
        url = "http://35.196.206.219:8080/v1/view"
        payload = {
            "type": "entry_function_payload",
            "function": f"0x4bf51972879e3b95c4781a5cdcb9e1ee24ef483e7d22f2d903626f126df62bd1::voting_escrow::get_voting_power_at_epoch",
            "type_arguments": [],
            "arguments": [
                f"{ve_token}",
                f"2829"
            ],
        }
        return requests.post(url, json=payload).json()[0]
    except Exception as e:
        print(f"ve_token: {ve_token} - error: {e}")
        return 0


def job(item, res):
    item = item.replace("\n", "")
    index = item.rfind(",")
    obj = item[1:index - 1]
    obj_data = json.loads(obj.replace("\"\"", "\""))
    weights = obj_data['weights']
    total_voted = get_total_voted(obj_data['ve_token']['inner'])
    print(obj_data['owner'])
    print(total_voted)
    if len(obj_data['pools']) == 1:
        if obj_data['pools'][0]['inner'] not in res:
            res[obj_data['pools'][0]['inner']] = int(total_voted)
        else:
            res[obj_data['pools'][0]['inner']] += int(total_voted)
    else:
        for i, pool in enumerate(obj_data['pools']):
            if pool['inner'] not in res:
                res[pool['inner']] = int(total_voted) * int(weights[i]) / 100
            else:
                res[pool['inner']] += int(total_voted) * int(weights[i]) / 100


res = {}
f1 = open("vote_result.csv", "r")
data = f1.readlines()
with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
    _ = [executor.submit(job, item, res) for item in data]
print(res)
f1 = open("vote_result_1.csv", "w")
for key in res:
    f1.write(f"{key},{res[key] / 10 ** 8}\n")